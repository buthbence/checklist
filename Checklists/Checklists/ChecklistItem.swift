//
//  ChecklistItem.swift
//  Checklists
//
//  Created by Buth Bence on 2018. 07. 29..
//  Copyright © 2018. BBence. All rights reserved.
//

import Foundation

class ChecklistItem: NSObject {
    
    var text = ""
    var checked = false
    
    func toggleChecked() {
        checked = !checked
    }
    
}
